Listen.
=======

----

Es gibt verschiedene Arten von Listen, die wichtigsten sind "normale" (mit ``*``, ``+`` oder ``-``), nummerierte (mit Zahlen oder ``#``) und Definitionen. Jeder Listenpunkt ist dabei ein eigener Block.

Normale und nummerierte Listen.
-------------------------------

..  admonition:: Beispiel

    ::

        * Normaler Listenpunkt.

          Zweiter Absatz des ersten Listenpunktes.

        - Noch ein normaler Listenpunkt.

        1. Erster nummerierter Listenpunkt.

        #. Zweiter nummerierter Listenpunkt mit automatisch generiertem Index.

           + normale und nummerierte Listen können…
           * … geschachtelt werden.

    * Normaler Listenpunkt.

      Zweiter Absatz des ersten Listenpunktes.

    - Noch ein normaler Listenpunkt.

    1. Erster nummerierter Listenpunkt.

    #. Zweiter nummerierter Listenpunkt mit automatisch generiertem Index.

       + normale und nummerierte Listen können…

       * … geschachtelt werden.

Definitionen.
-------------

Definitionen bestehen aus einem Begriff/einer Wortgruppe und ihrer (eingerückten) Erklärung. Die Erklärung ist ein eigener Block.

..  admonition:: Beispiel

    ::

        Begriff 1
            Absatz 1 der Erklärung zu Begriff 1.

            Absatz 2.

        Begriff 2
            * Die Erklärungen können,
            * wie jeder Block,
            * auch Tabellen, Listen u.Ä. enthalten.

    Begriff 1
        Absatz 1 der Erklärung zu Begriff 1.

        Absatz 2.

    Begriff 2
        * Die Erklärungen können,
        * wie jeder Block,
        * auch Tabellen, Listen u.Ä. enthalten.


----

:doc:`« reST -- Blöcke <rest-blöcke>` · :doc:`reST -- Tabellen » <rest-tabellen>`
