Danke fürs Zuhören!
===================

----

..  figure:: images/lab--small.png
    :align: right

Dieses Tutorium findet ihr unter :file:`/otto/share/patrice/tutorials/laborbuch-mit-sphinx-tutorium/index.html`.

Eine ausführlichere Anleitung, die ich in Zukunft noch verbessern werde, befindet sich unter :file:`/otto/share/patrice/tutorials/laborbuch-mit-sphinx/index.html`. Sphinx (und reST) können nämlich noch einiges mehr.

Mein eigenes Laborbuch findet ihr unter :file:`/otto/share/patrice/lab-notebook`.

Anregungen, Fragen und Drohungen persönlich oder an patrice.peterson@student.uni-halle.de.

Erweiterungen.
--------------

Ich werde aller Voraussicht nach noch ein oder zwei Erweiterungen schreiben und diese auf `bitbucket.org/runiq <http://bitbucket.org/runiq>`_ freigeben. Mit diesen sollte es möglich sein…

* … von einer Literaturliste im BibTeX-Format alle verwendeten Literaturverweise in einem Sphinx-Dokument zusammenzufassen.
* … chemische Formeln einfacher zu schreiben.

----

:doc:`« Texteditoren <editoren>` · :doc:`Inhalt » <index>`
