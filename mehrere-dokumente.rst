Mehrere Dokumente.
==================

----

Irgendwann passt nicht mehr alles in ein Dokument, dann ist es Zeit, auszulagern.

..  code-block:: sh

    $ ls sphinx-tutorium
    conf.py     index.rst   Makefile
    …
    installation.rst        konfiguration.rst 
    projektstruktur.rst     rest-grundlagen.rst
    …

Die neuen Dateien werden ins Inhaltsverzeichnis der Masterdatei (hier: :file:`index.rst`) hinzugefügt. Das Inhaltsverzeichnis wird durch die ``toctree``-Direktive festgelegt. ::

    ..  toctree::

        …
        installation
        konfiguration
        projektstruktur
        rest-grundlagen
        …


----

:doc:`« Bauen der Dokumentation <bauen>` · :doc:`Texteditoren » <editoren>`
