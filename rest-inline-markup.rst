*Inline*-Auszeichnung.
======================

----

Diese versehen ein Wort oder eine Wortgruppe mit einem bestimmten Aussehen oder einer bestimmten Funktion.

``*Text*`` hebt etwas *hervor*.

``**Text**`` hebt etwas **stark hervor**.

````Text```` stellt etwas ``wortgetreu dar``.

Auszeichnung mit Rollen.
------------------------

Die vielseitigste *inline*-Auszeichnung ist ```Text```. Damit wird etwas ganz allgemein für die Interpretation durch Sphinx markiert. Wie es interpretiert wird, hängt ab von der *Rolle*, mit der die Wortgruppe versehen wird. ::

    :rolle:`zu interpretierender Text`

============================================ ===================================
Rolle                                        Bedeutung
============================================ ===================================
``:emphasis:`Text```                         Für *hervorgehobenen* Text.
``:strong:`Text```                           Für **stark hervorgehobenen** Text.
``:literal:`Text```                          Für ``wortgetreuen`` Text.
``:subscript:`Text``` oder ``:sub:`Text```   Für :sub:`tiefgestellten` Text.
``:superscript:`Text``` oder ``:sup:`Text``` Für :sup:`hochgestellten` Text.
============================================ ===================================

``*text*``, ``**text**`` und ````text```` sind verkürzte Schreibweisen für die (häufig benutzten) Rollen ``:emphasis:``, ``:strong:`` und ``:literal:``.

Rollen sind eine von zwei Möglichkeiten zur Erweiterung von reST. Sphinx bringt selbst einige zusätzliche Rollen mit:

================================== ====================================================================================================================
Rolle                              Bedeutung
================================== ====================================================================================================================
``:abbr:`Abk. (Abkürzung)```       Beim mit der Maus drüberfahren erscheint der Text in Klammern: :abbr:`Abk. (Abkürzung)`.
``:download:`Datei.jpg```          Liefert einen Verweis zu einer Datei: :download:`images/lab--small.png`.
``:doc:`Dateiname```               Liefert einen Verweis auf das Dokument :file:`Dateiname.rst` bzw. :file:`Dateiname.txt`: :doc:`index`.
``:ref:`Zielort```                 Liefert einen Verweis auf den Zielort (der an der jeweiligen Stelle angegeben werden muss).
================================== ====================================================================================================================

Die Rollen ``:download:``, ``:doc:`` und ``:ref:`` werden im nächsten Abschnitt näher beschrieben.

----

:doc:`« reST -- Tabellen <rest-tabellen>` · :doc:`reST -- Ausdrückliche Auszeichnung » <rest-explicit-markup>`
