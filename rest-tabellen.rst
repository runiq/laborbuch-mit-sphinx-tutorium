Tabellen.
=========

----

ReST-Tabellen können auf einfache oder komplexere Weise geschrieben werden.

Die komplexe Variante unterstützt mehr Features, dafür muss das Gitter selbst gezeichnet werden.


..  admonition:: Beispiel

    ::

        Einfach:               Komplex:

        ===== ===== =======    +---------+----------+------------------+
          Eingabe   Ausgabe    | Kopf 1  | Kopf 2   | Kopf 3           |
        ----------- -------    +=========+==========+==================+
          A     B   A od. B    | Zeile 1 | Spalte 2 | Spalte 3         |
        ===== ===== =======    +---------+----------+------------------+
        False True  True       | Zeile 2 | Über mehrere Spalten        |
        True  True  True       +---------+----------+------------------+
        ===== ===== =======    |         |          | ======= ======== |
                               | Zeile 3 | Über     |  Jede     Zelle  |
                               |         |          | ======= ======== |
                               +---------+ mehrere  |   ist     ein    |
                               | Zeile 4 |          |      Block       |
                               |         | Zeilen   | ================ |
                               +---------+----------+------------------+

    Einfach:

    ===== ===== =======
      Eingabe   Ausgabe
    ----------- -------
      A     B   A od. B
    ===== ===== =======
    False True  True
    True  True  True
    ===== ===== =======

    Komplex:

    +---------+----------+------------------+
    | Kopf 1  | Kopf 2   | Kopf 3           |
    +=========+==========+==================+
    | Zeile 1 | Spalte 2 | Spalte 3         |
    +---------+----------+------------------+
    | Zeile 2 | Über mehrere Spalten        |
    +---------+----------+------------------+
    |         |          | ======= ======== |
    | Zeile 3 | Über     |  Jede     Zelle  |
    |         |          | ======= ======== |
    +---------+ mehrere  |   ist     ein    |
    | Zeile 4 |          |      Block       |
    |         | Zeilen   | ================ |
    +---------+----------+------------------+

----

:doc:`« reST -- Listen <rest-listen>` · :doc:`reST -- inline-Auszeichnung » <rest-inline-markup>`
