Bauen der Dokumentation.
========================

----

Um zu sehen, wie das fertige Projekt aussieht, muss man es *kompilieren* (bauen).

Dies geschieht mit Hilfe der Dateien :file:`make.bat` (Windows) bzw. :file:`Makefile` (Linux) im Wurzelverzeichnis.

Man ruft sie mit dem Format auf, das man bauen will. Eine Liste der unterstützten Formate bietet ``make help``.

..  admonition:: Beispiel

    ..  code-block:: sh

        $ make html
        sphinx-build -b html   . _build/html
        Running Sphinx v1.1.3
        loading translations [de]... done
        loading pickled environment... done
        building [html]: targets for 1 source files that are out of date
        updating environment: 1 added, 1 changed, 0 removed
        reading sources... [100%] schnellstart
        looking for now-outdated files... none found
        pickling environment... done
        checking consistency... done
        preparing documents... done
        writing output... [100%] schnellstart
        writing additional files... genindex search
        copying static files... done
        dumping search index... done
        dumping object inventory... done
        build succeeded, 1 warning.

        Build finished. The HTML pages are in _build/html.

----

:doc:`« reST -- Fallstricke <rest-fallstricke>` · :doc:`Mehrere Dokumente » <mehrere-dokumente>`
