Grundbausteine.
===============

----

Die reST-Syntax besteht aus folgenden Bausteinen:

..  hlist::

    * Überschriften
    * Absätze
    * Listen
    * Tabellen
    * Blöcke
    * Verweise
    * *Inline*-Auszeichnungen
    * Ausdrückliche Auszeichnungen

Es gibt mehrere Arten von Listen, Blöcken, Verweisen und Auszeichnungen.

Über neue *inline*- oder ausdrückliche Auszeichnungen lässt sich reST erweitern.

----

:doc:`« Projektstruktur <projektstruktur>` · :doc:`reST -- Überschriften und Absätze » <rest-gliederung>`
