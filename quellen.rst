Bildquellen.
============

----

:download:`lab--small.png <images/lab--small.png>`: http://whiteappleer.blogspot.de/2010/11/icon-collection-002.html

:download:`peak-productivity-2am.png <images/peak-productivity-2am.png>`: http://www.phdcomics.com/comics.php?f=1219

:download:`rest.png <images/rest.png>`: http://docutils.sourceforge.net/rst.html

:download:`sphinx.png <images/sphinx.png>`: http://sphinx.pocoo.org/

:download:`syntax-highlighting.png <images/syntax-highlighting.png>`: https://de.wikipedia.org/w/index.php?title=Syntaxhervorhebung&oldid=102031815

:download:`twilight-sparkle.jpg <images/twilight-sparkle.jpg>`: http://images.wikia.com/twitterponies/images/4/47/TwilightSparkle.jpg

----

:doc:`^ Inhalt <index>`
