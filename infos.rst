Infos.
======

----

Quelltext.
----------

Der Quelltext für dieses Tutorial ist `hier <http://bitbucket.org/runiq/laborbuch-mit-sphinx-tutorium>`_ zu finden. Er ist mit `Mercurial <http://mercurial.selenic.com>`_ unter Versionskontrolle gehalten.

Links.
------

Das Sphinx-Projekt findet ihr unter `sphinx.pocoo.org <http://sphinx.pocoo.org>`_.

Informationen über reST gibt es auf `docutils.sourceforge.net/rst.html <http://docutils.sourceforge.net/rst.html>`_.

Theme.
------

Das in diesem Tutorium benutzte Sphinx-Theme ist eine leicht abgewandelte Version von `kr <https://github.com/kennethreitz/kr-sphinx-themes>`_ ohne Seitenleiste und Fußzeile.

``kr`` ist wiederum eine leicht abgewandelte Version des `Flask-Themes <http://flask.pocoo.org>`_.

----

:doc:`^ Inhalt <index>`
