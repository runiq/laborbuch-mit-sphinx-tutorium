Schnellstart.
=============

----

#. Sphinx installieren.
#. ``sphinx-quickstart`` ausführen.
#. In :file:`conf.py` folgende Parameter anpassen:
    * ``language``
    * ``html_theme``
    * ``html_title``
#. Schreiben und neue Datei ins Inhaltsverzeichnis einfügen.
#. Mit ``make html`` im Wurzelverzeichnis bauen.
#. Zurück zu Schritt 4.

----

:doc:`« Sphinx und reST <sphinx-und-rest>` · :doc:`Installation » <installation>`
