..  Sphinx-Tutorium documentation master file, created by
    sphinx-quickstart on Fri May 25 09:30:48 2012.
    You can adapt this file completely to your liking, but it should at least
    contain the root `toctree` directive.

Laborbuch schreiben mit Sphinx.
===============================

----

..  figure:: images/lab--small.png
    :align: right

    © 2012, `Patrice Peterson <runiq@archlinux.us>`_.

    :doc:`infos` :doc:`quellen` :doc:`lizenz`

:doc:`sphinx-und-rest`

Sphinx

:doc:`schnellstart` | :doc:`installation` | :doc:`konfiguration` | :doc:`projektstruktur`

reST

:doc:`rest-elemente` | :doc:`rest-gliederung` | :doc:`rest-blöcke` | :doc:`rest-listen` | :doc:`rest-tabellen` | :doc:`rest-inline-markup` | :doc:`rest-explicit-markup` | :doc:`rest-verweise` | :doc:`rest-fallstricke`

Mehr Sphinx

:doc:`bauen` | :doc:`mehrere-dokumente` 

Nachwort

:doc:`editoren` | :doc:`ausblick`

----

:doc:`« Ausblick <ausblick>` | :doc:`Sphinx und reST » <sphinx-und-rest>`

..  toctree::
    :hidden:

    sphinx-und-rest
    schnellstart
    installation
    konfiguration
    projektstruktur
    rest-elemente
    rest-gliederung
    rest-blöcke
    rest-listen
    rest-tabellen
    rest-inline-markup
    rest-explicit-markup
    rest-verweise
    rest-fallstricke
    bauen
    mehrere-dokumente
    editoren
    ausblick
    infos
    quellen
    lizenz
