Projektstruktur.
================

----

Nach erfolgtem ``sphinx-quickstart``-Aufruf hat man vier Dateien und ein paar (uninteressante) Ordner im Verzeichnis.

:file:`conf.py`
    Konfigurationsdatei.
:file:`index.rst`
    Wurzeldokument mit Haupt-Inhaltsverzeichnis.
:file:`Makefile` und :file:`make.bat`
    Skripte zum Bauen der Dokumentation unter Windows (:file:`make.bat`) und Linux (:file:`Makefile`).

Es können noch viele zusätzliche Dateien hinzukommen; z.B. Bilder oder andere Sphinx-Dokumente, die man in der :file:`index.rst` einbindet. Auch das Zugreifen auf Dateien in Unterordnern ist problemlos möglich:

* Ein Bild :file:`bild.png` im Unterordner :file:`bilder` lässt sich z.B. durch ``.. image:: bilder/bild.png`` darstellen.
* Eine Datei :file:`unterdokument` im Ordner :file:`kapitel2` lässt sich beispielsweise so ins Inhaltsverzeichnis einbinden::

    ..  toctree::

        kapitel2/unterdokument

Zu beiden später mehr.

----

:doc:`« Konfiguration <konfiguration>` · :doc:`reST -- Grundbausteine » <rest-elemente>`
