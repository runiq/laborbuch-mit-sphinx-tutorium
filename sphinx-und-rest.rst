Sphinx und reST.
================

----

..  image:: images/sphinx.png

..  pull-quote::

    "Sphinx ist ein Werkzeug zur Übersetzung von reStructuredText-Dateien in verschiedene Ausgabeformate. Es legt dabei automatisch Querverweise, Indizes usw. an."

    -- `Sphinx-Dokumentation <file:///usr/share/doc/python-sphinx/intro.html>`_

Sphinx war ursprünglich da zur Dokumentation des Python-Projekts, später auch allgemeiner für andere Programmierprojekte. Zu Sphinx' Features zählen:

* **Ausgabeformate:** HTML (auch Windows-Hilfedateien), LaTeX (für druckbare PDF-Versionen), einfacher Text, u.v.m.
* **Umfassende Vernetzungsmöglichkeiten:** auf andere Dokumente, auf andere Sphinx-Projekte, Glossar und Index, Literaturverweise.
* **Korrekter Umgang mit Code:** Automatische Syntax-Hervorhebung durch `Pygments <http://pygments.org>`_.
* **Erweiterungen:** Mathematische Formeln, Erstellung von Strukturdiagrammen, automatisiertes Testen von Code, etc.

..  image:: images/rest.png

..  pull-quote::

    "reStructuredText (kurz: ReST, reST oder RST) ist eine vereinfachte Auszeichnungssprache (Markup) mit dem Ziel, in der reinen Textform besonders lesbar zu sein. Weiterhin soll reStructuredText leicht in andere Formate umwandelbar sein."

    -- `Wikipedia <https://de.wikipedia.org/w/index.php?title=ReStructuredText&oldid=101346123>`_

ReST schreiben ist ein wenig wie Programmieren.

..  admonition:: Beispiel

    ::

        reStructuredText.
        =================

        ..  image:: images/rest.png

        ..  pull-quote::

            reStructuredText (kurz: ReST, reST oder RST) ist eine vereinfachte
            Auszeichnungssprache (Markup) mit dem Ziel, in der reinen Textform
            besonders lesbar zu sein. Weiterhin soll reStructuredText leicht in
            andere Formate umwandelbar sein.

            -- `Wikipedia <https://de.wikipedia.org/wiki/ReStructuredText>`_

        ReST schreiben ist ein wenig wie Programmieren.

Zusammenhang zwischen beiden.
-----------------------------

ReST ist die Grundlage, auf der Sphinx aufbaut. Es bietet die Strukturelemente, aus denen Sphinx nach einem vorgegebenen Schema (*Theme*) die Dokumentation zusammenbaut.

Sphinx erweitert reST an einigen Stellen. Über *Extensions* kann Sphinx selbst noch erweitert werden.

----

:doc:`« Inhalt <index>` · :doc:`Schnellstart » <schnellstart>`
