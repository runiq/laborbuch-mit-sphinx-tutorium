Ausdrückliche Auszeichnung.
===========================

----

Die ausdrücklichen Auszeichnungen werden für Sachen benutzt, die…

* … kein wirkliches Äquivalent auf dem Papier haben (Verweisziele)
* … aus dem normalen Textfluss herausfallen (Abbildungen)
* … besonderer Behandlung durch Sphinx bedürfen (Direktiven)

Sie beginnen mit zwei Punkten und einem Leerzeichen. ::

    .. <auszeichnung>

Die Verweisziele werden auf der nächsten Seite behandelt.

Direktiven.
-----------

Neben den *Rollen* sind die *Direktiven* der zweite Weg, über den reST erweitert werden kann. Sphinx definiert unzählige neue Direktiven, von denen hier nur ein paar gezeigt werden sollen.

Alle Direktiven haben die gleiche Syntax::

    .. name_der_direktive:: <optionaler Text>
       :option1: wert1
       :option2: wert2
       :option3: wert3

       Dies ist der Block, der mit Funktionen versehen wird.

       Er kann mehrere Absätze haben und bei manchen Direktiven auch leer sein.

Optionen und Blocktext müssen gleich weit eingerückt sein. Keine Option ist zwingend.


Bilder.
^^^^^^^

Endlich Bilder!

============ ==============================================================================================
Option       Bedeutung
============ ==============================================================================================
``:align:``  Links-, rechtsbündig oder zentriert. Mit ``left`` oder ``right`` fließt der Text um sie herum.
``:width:``  Die Breite, die das Bild haben soll, in Prozent (``%``) oder Pixeln (``px``).
``:height:`` Die Höhe, die das Bild haben soll.
``:scale:``  Der Maßstab des Bildes in Prozent (``%``).
============ ==============================================================================================

..  admonition:: Beispiel

    ::

        .. image:: images/lab--small.png
           :align: center
           :scale: 50%

    ..  image:: images/lab--small.png
        :align: center
        :scale: 50%

Abbildungen.
^^^^^^^^^^^^

..  figure:: images/peak-productivity-2am.png
    :align: right
    :figwidth: 50%

    Auf diesem Bild ist meine Produktivität in Abhängigkeit von der Zeit dargestellt.

Diese sind im Gegensatz zu Bildern aus dem Fließtext herausgelöst.

::

    .. figure:: pfad-zur-datei.jpg
       :figwidth: 50%
       <Rest wie bei der "image"-Direktive>

       Hier steht die Bildunterschrift.

       Wie jeder Block kann sie
       mehrere Absätze haben.

============== =============================================================================
Option         Bedeutung
============== =============================================================================
``:figwidth:`` Welchen Teil der Textbreite die Abbildung einnehmen soll, in Prozent (``%``).
============== =============================================================================

..  admonition:: Beispiel

    ::

        ..  figure:: images/peak-productivity-2am.png
            :align: right
            :figwidth: 50%

    Siehe rechts.

Informationskästen.
^^^^^^^^^^^^^^^^^^^

Umgeben einen Block mit einem Kasten.

..  admonition:: Beispiel

    ::

        .. admonition:: Beispiel

           Auf genau dieselbe Weise sind die Beispielkästen in diesem Dokument entstanden.

    Auf diese Weise sind die Beispielkästen in diesem Dokument entstanden.

Inhaltsverzeichnis.
^^^^^^^^^^^^^^^^^^^

Auch das Inhaltsverzeichnis ist eine Direktive. Es legt fest, in welcher Reihenfolge die Dateien in der Seitenleiste verlinkt werden.

Normalerweise ist es in der Masterdatei enthalten, es kann aber in beliebigen Dokumenten angegeben werden.

============== ================================================================================================================
Option         Bedeutung
============== ================================================================================================================
``:maxdepth:`` Bis zu welcher Ebene die Überschriften des jeweiligen Dokuments ins Inhaltsverzeichnis integriert werden sollen.
``:hidden:``   Optional. Wenn ihr euer eigenes Inhaltsverzeichnis erstellen wollt, könnt ihr das "offizielle" auch verstecken.
============== ================================================================================================================

..  admonition:: Beispiel

    ::

        .. toctree::
           :maxdepth: 2

           installation
           konfigration
           schnellstart
           bauen

    .. toctree::
       :maxdepth: 2

       installation
       konfiguration
       schnellstart
       bauen

Weitere Direktiven.
^^^^^^^^^^^^^^^^^^^

Mit Direktiven kann man noch viel mehr machen:

* CSV-Dateien im Tabellenformat anzeigen
* Mathematische Gleichungen setzen
* Tabellen mit Namen versehen
* Seitenleisten basteln
* u.v.m

Alle Direktiven sind in der reST- bzw. Sphinx-Dokumentation erläutert.

----

:doc:`« reST -- inline-Auszeichnung <rest-inline-markup>` · :doc:`reST -- Verweise » <rest-verweise>`
