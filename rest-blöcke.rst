Blöcke.
=======

----

Mit "Block" wird ein in sich abgeschlossener Textbaustein bezeichnet.

Blöcke werden durch Einrückung definiert. Alles, was tiefer eingerückt ist als vorhergehender Text, gehört zu einem neuen Block. Wenn ein neuer Absatz beginnt, der nicht mehr so tief eingerückt ist, gilt der Block als beendet. ::

    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy
    eirmod tempor invidunt ut labore et dolore magna aliquyam erat.

        Dies ist ein neuer Block, weil er weiter eingerückt ist als der
        vorherige Text.

    Hier geht der "Lorem ipsum"-Block weiter.

Neben dem "allgemeinen" Block gibt es mehrere spezielle Arten. Hier soll nur einer vorgestellt werden.

Block mit wortgetreuer Darstellung.
-----------------------------------

Dieser Block wird mit ``::`` eingeleitet. Sein Inhalt wird von Sphinx nicht interpretiert und in einer Festbreitenschrift dargestellt. Nützlich für Skripte, da Sphinx automatische Syntaxhervorhebung unterstützt.

..  admonition:: Beispiel

    ::

        Das Folgende Skript ist ein Testskript. ::

            #!/bin/sh
            # Testskript mit Schleife
            i=1
            while [ $i -le 5 ]
            do
              echo $i
              i=`expr $i + 1`
            done

        Mit diesem Absatz gilt der eingerückte Block als beendet.

    ..  highlight:: sh

    Das Folgende Skript ist ein Testskript. ::

        #!/bin/sh
        # Testskript mit Schleife
        i=1
        while [ $i -le 5 ]
        do
          echo $i
          i=`expr $i + 1`
        done

    Mit diesem Absatz gilt der eingerückte Block als beendet.

----

:doc:`« reST -- Überschriften und Absätze <rest-gliederung>` · :doc:`reST -- Listen » <rest-listen>`
