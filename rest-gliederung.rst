Überschriften und Absätze.
==========================

----

Überschriften werden in reST erzeugt durch Unterstreichen (und, wahlweise, Überstreichen). Dafür kann jedes beliebige nicht-alphanumerische Zeichen verwendet werden, und reST findet die Hierarchie von selbst heraus.

Absätze werden durch Freilassen einer Zeile erzeugt. Ein normaler Zeilenumbruch wird von reST ignoriert. ::

    =============
    Überschrift 1
    =============

    Ich bin ein Absatz von Überschrift 1.
    Auch ich gehöre noch zum ersten Absatz.

    Hier beginnt der zweite Absatz.
    Auch dieser Satz gehört noch zum zweiten Absatz.

    Unterüberschrift
    ****************

    Unterunterüberschrift
    ---------------------

    =============
    Überschrift 2
    =============


----

:doc:`« reST -- Grundbausteine <rest-elemente>` · :doc:`reST -- Blöcke » <rest-blöcke>`
