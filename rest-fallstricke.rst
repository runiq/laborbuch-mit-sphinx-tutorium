Fallstricke.
============

----

Einrückungen.
-------------

Bei falscher Einrückung erkennt Sphinx nicht, was zu welchem Block gehört.

..  admonition:: Falsch

    ::

        ..  image:: pfad/zur/bilddatei.jpg
        :width: 50%

..  admonition:: Richtig

    ::

        ..  image:: pfad/zur/bilddatei.jpg
            :width: 50%

Das gilt für alle *Direktiven* und außerdem auch für Listen. ::

    * Dies ist ein Listenpunkt.

      Er hat mehrere Absätze.

    Dieser Absatz gehört nicht mehr zur Liste.

    * Dieser Listenpunkt gehört zu einer neuen Liste.

Escaping.
---------

*Escaping* hält Sphinx davon ab, Sachen zu interpretieren.

..  admonition:: Beispiel

    Man würde gern \*Protein\* schreiben, aber Sphinx interpretiert das als *Protein*. Die Lösung::

        *Protein* wird durch Escapen mit Backslashes zu \*Protein\*.

    *Protein* wird durch Escapen mit Backslashes zu \*Protein\*.

Auszeichnungen *in* einem Wort.
-------------------------------

Innerhalb eines Wortes müssen Auszeichnungen mit *Pseudo-Leerzeichen* umgeben werden.

..  admonition:: Beispiel

    2 H\ :sub:`2`\ O\ :sub:`2` → 2 H\ :sub:`2`\ O + O\ :sub:`2`

    ..  rubric:: Falsch

    ::

        2 H:sub:`2`O:sub:`2` → 2 H:sub:`2`O + O:sub:`2`

    2 H:sub:`2`O:sub:`2` → 2 H:sub:`2`O + O:sub:`2`

    ..  rubric:: Richtig

    ::

        2 H\ :sub:`2`\ O\ :sub:`2` → 2 H\ :sub:`2`\ O + O\ :sub:`2`

    2 H\ :sub:`2`\ O\ :sub:`2` → 2 H\ :sub:`2`\ O + O\ :sub:`2`

Gruselig, ich weiß.

Verschachtelte Auszeichnungen.
------------------------------

Dies ist leider (noch) nicht möglich. ::

    Dies soll ein hervorgehobener :doc:`*Verweis* <zielort>` auf ein bestimmtes
    Dokument sein.

Stattdessen kommt:

    Dies soll ein hervorgehobener :doc:`*Verweis* <zielort>` auf ein bestimmtes Dokument sein.

----

:doc:`« reST -- Verweise <rest-verweise>` · :doc:`Bauen » <bauen>`
