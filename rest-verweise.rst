Verweise.
=========

----

Verweise dienen der Quervernetzung. Man kann in Sphinx auf Folgendes verweisen:

* Literaturstellen
* Fußnoten
* URLs und E-Mail-Adressen
* Dateien
* Sphinx-Dokumente
* Beliebige Stellen im Sphinx-Projekt
* Andere Sphinx-Projekte (mit Erweiterung)

Literatur und Fußnoten.
-----------------------

Fußnoten und Literaturstellen sind sehr ähnlich in ihrer Syntax und unterscheiden sich nur in ihrem "Bezeichner". ::

    Blablabla [<Bezeichner>]_ blabla

    ...

    ..  [<Bezeichner>] Erläuterung

Bei Literaturstellen fängt der Bezeichner mit einem Buchstaben an.

Fußnoten haben als Bezeichner entweder:

* eine Zahl für manuelle Nummerierung,
* ``#`` bzw. ``#name`` für automatische Nummerierung (und Versehung mit Namen),
* ``*`` für automatische Versehung mit Symbolen.

..  admonition:: Beispiel

    ..  rubric:: Literaturstelle

    ::

        Diese Methode wird in [Maier.etal2003]_ beschrieben.

        Literaturverzeichnis
        --------------------

        ..  [Maier.etal2003] Maier R, “Tomatenpüree und seine vielfältigen
            Anwendungen”, J. Med. Sci. (2003), S. 309–312.

    Diese Methode wird in [Maier.etal2003]_ beschrieben.

    ..  rubric:: Literaturverzeichnis

    ..  [Maier.etal2003] Maier R, “Tomatenpüree und seine vielfältigen
                               Anwendungen”, J. Med. Sci. (2003), S. 309–312.

    ..  rubric:: Fußnote

    ::

        In ein Eppi passen bis zu 3 mg DNA. [#eppi]_

        Fußnoten
        --------

        ..  [#eppi] Für sehr hohe Drücke und sehr geringe Werte von 3.

    In ein Eppi passen bis zu 3 mg DNA. [#eppi]_

    ..  rubric:: Fußnoten

    ..  [#eppi] Für sehr hohe Drücke und sehr geringe Werte von 3.

URLs und E-Mail-Adressen.
-------------------------

Die Syntax für diese ist ähnlich zu Fußnoten. Wenn allerdings eine ganze Wortgruppe verlinkt werden soll, muss diese mit Backticks umgeben werden.

..  admonition:: Beispiel

    ::

        Ich bin eine URL_. Und ich `eine E-Mail-Adresse`_.

        ...

        ..  _URL: http://www.example.com

        ..  _eine E-Mail-Adresse: max.mustermann@example.com

    Ich bin eine URL_. Und ich `eine E-Mail-Adresse`_.

    ..  _URL: http://www.example.com

    ..  _eine E-Mail-Adresse: max.mustermann@example.com

Wenn man das Ziel des Links lieber an Ort und Stelle hat, kann man es *einbetten*.

..  admonition:: Beispiel

    ::

        Ich bin eine `URL <http://www.example.com>`_.
        Und ich `eine E-Mail-Adresse <max.mustermann@example.com>`_.

    Ich bin eine `URL <http://www.example.com>`_.
    Und ich `eine E-Mail-Adresse <max.mustermann@example.com>`_.

Andere Dateien.
---------------

Mit den bereits erwähnten *Rollen* ``:doc:``, ``:download:`` und ``:ref:`` lässt sich auf andere Dateien verweisen.

Die Syntax ist sehr ähnlich zu der für eingebettete URLs und E-Mails. ::

    :rolle:`Text, der da stehen soll <zielort>`


..  admonition:: Beispiel

    ::

        Ich bin ein Verweis auf :doc:`anderes reST-Dokument <index>`.

        Dies ist einer auf :download:`beliebige Datei <images/lab--small.png>`, die
        im Browser geöffnet wird.

        Und hier ist einer auf :ref:`eine beliebige Textstelle <textstelle>`, die
        ich vorher markieren muss.

        ..  _textstelle:

        Hier ist diese Textstelle.

    Ich bin ein Verweis auf :doc:`anderes reST-Dokument <index>`.

    Dies ist einer auf :download:`beliebige Datei <images/lab--small.png>`, die im Browser geöffnet wird.

    Und hier ist einer auf :ref:`eine beliebige Textstelle <textstelle>`, die ich vorher markieren muss.

    ..  _textstelle:

    Hier ist diese Textstelle.

Übersicht.
----------

+---------------------------------+-------------------------------------+------------------------------------------------+
| Verweis                         | Aussehen                            | Ziel                                           |
+=================================+=====================================+================================================+
| Fußnoten                        | ``[#]_``                            | ``.. [#] Text``                                |
|                                 +-------------------------------------+------------------------------------------------+
|                                 | ``[#bezeichner]_``                  | ``.. [#bezeichner] Text``                      |
|                                 +-------------------------------------+------------------------------------------------+
|                                 | ``[*]_``                            | ``.. [*] Text``                                |
+---------------------------------+-------------------------------------+------------------------------------------------+
| Literaturstellen                | ``[bezeichner]_``                   | ``.. [bezeichner] Text``                       |
|                                 +-------------------------------------+                                                +
|                                 | ``bezeichner_``                     |                                                |
|                                 +-------------------------------------+                                                +
|                                 | ``bEzEicHNeR_``                     |                                                |
+---------------------------------+-------------------------------------+------------------------------------------------+
| URLs/E-Mail-Adressen            | ``Wort_``                           | ``.. _Wort: http://www.example.com``           |
|                                 +-------------------------------------+------------------------------------------------+
|                                 | ```Mehrere Wörter`_``               | ``.. _Mehrere Wörter: http://www.example.com`` |
|                                 +-------------------------------------+------------------------------------------------+
|                                 | ```Text <www.example.com>`_``                                                        |
+---------------------------------+-------------------------------------+------------------------------------------------+
| Dateien                         | ``:download:`Text <pfad/zur/datei>```                                                |
+---------------------------------+-------------------------------------+------------------------------------------------+
| Sphinx-Dokumente                | ``:doc:`Text <pfad/zur/datei>```                                                     |
+---------------------------------+-------------------------------------+------------------------------------------------+
| Beliebige Stellen               | ``:ref:`Text <zielort>```           | ``.. _zielort:``                               |
+---------------------------------+-------------------------------------+------------------------------------------------+

----

:doc:`« reST -- Ausdrückliche Auszeichnung <rest-explicit-markup>` · :doc:`reST -- Fallstricke » <rest-fallstricke>`
