Ein Wort zu Texteditoren.
=========================

----

Notepad ist nicht geeignet zum Verfassen von Sphinx-Dokumenten. Word und OpenOffice erst recht nicht. Ein guter Texteditor (oder eine gute Entwicklungsumgebung :o) vereinfacht das Arbeiten ungemein.

Interessante Features.
----------------------

..  figure:: images/syntax-highlighting.png
    :align: right
    :figwidth: 50%

    Editor mit Syntaxhervorhebung und Zeilennummerierung.

* Syntax-Hervorhebung.
* Zeilennummerierung.
* Mehrere Dokumente in einem Editorfenster.
* Bauen der Dokumentation direkt aus dem Editor.
* Anzeigen von Fehlern, die beim Bauen auftreten.
* *Snippets*: Wiederverwendbare Textbausteine.

Beispiele.
----------

**Eclipse:** Hat ein `reST-Plugin <http://marketplace.eclipse.org/content/rest-editor>`_.

**Geany:** Klein, aber hat die nötigen Features.

**Gedit:** Hat ebenfalls ein `reST-Plugin <http://kib2.alwaysdata.net/GEdit>`_.

**Notepad++:** Ähnelt Notepad, hat aber *viel* mehr Funktionen.

**Sublime Text 2:** Kostenpflichtig, viele innovative Funktionen (z.B. herausgezoomte Übersicht über das gesamte Dokument).

----

:doc:`« Mehrere Dokumente <mehrere-dokumente>` · :doc:`Ausblick » <ausblick>`
