Konfiguration.
==============

----

Ein Aufruf von ``sphinx-quickstart`` (hier verkürzt) initialisiert das Projekt. ::

    > Root path for the documentation [.]: Laborbuch
    > Author name(s): Patrice Peterson
    > Project version: 1
    > Source file suffix [.rst]:
    > Create Makefile? (Y/n) [y]:
    > Create Windows command file? (Y/n) [y]:

Nach Ende der Fragestunde hat man vier Dateien und ein paar (uninteressante) Ordner im Wurzelverzeichnis. Die Konfiguration erfolgt über :file:`conf.py`.

..  code-block:: python

    # Deutsche Sprache
    language = "de"
    # Schöneres Aussehen
    html_theme = "nature"
    # Passender Titel
    html_title = "Laborbuch von Patrice"
    # Bild
    html_logo = "bilder/lab--small.png"

Es sind noch viele weitere Einstellungen möglich.

----

:doc:`« Installation <installation>` · :doc:`Projektstruktur » <projektstruktur>`
