Installation.
=============

----

Das mache ich mit den Interessierten einzeln. Die nötige Software liegt unter :file:`/otto/share/patrice/laborbuch-mit-sphinx--software`.

Windows.
--------

#. Python (2.6 aufwärts) installieren.
#. ``PYTHON_HOME``- und ``Path``-Umgebungsvariablen anpassen.
#. ``distribute``-Paket installieren.
#. In einem Terminal Folgendes eingeben::

    easy_install sphinx

Cygwin.
-------

#. Python (2.6 aufwärts) installieren.
#. Folgendes in einem Cygwin-Terminal eingeben::

    $ curl -O http://python-distribute.org/distribute_setup.py
    $ python distribute_setup.py
    $ easy_install sphinx

Linux.
------

Mit root-Rechten:

#. Paketmanager bemühen

Ohne root-Rechte:

#. Python kompilieren (``./configure && make && make PREFIX="~/.local" install``)
#. Folgendes in einem Terminal eingeben::

    $ curl -O http://python-distribute.org/distribute_setup.py
    $ python distribute_setup.py
    $ easy_install sphinx

----

:doc:`« Sphinx und reST <sphinx-und-rest>` · :doc:`Konfiguration » <konfiguration>`
